import discord
import sqlite3

YOUR_USER_ID = 'your_discord_user_id'  # Replace with your own discord User ID
YOUR_BOT_TOKEN = 'your_bots_token'  # Replace with your own bots token

# using sqlite to store the messages on the server/computer where the bot is running
# replace with a txt/csv file dump if you don't want to use a schema
conn = sqlite3.connect('messages.db')
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS messages
             (id INTEGER PRIMARY KEY, content TEXT, author_id TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''')
conn.commit()

#configuring minimal bot intents
intents = discord.Intents.default()
intents.messages = True
intents.message_content = True

#setting up bot client
client = discord.Client(intents=intents)

# simple logged confirmation the bot is working
@client.event
async def on_ready():
    print(f'Logged in as {client.user}')

# actual message logging logic
@client.event
async def on_message(message):
    if message.author.id == YOUR_USER_ID:  # validate the message is yours
        # log the message to db
        # this is where you'd change how you want your messages logged if you aren't using sqlite
        c.execute("INSERT INTO messages (content, author_id) VALUES (?, ?)",
                  (message.content, str(message.author.id)))
        conn.commit()


client.run(YOUR_BOT_TOKEN)
